class Question {
    constructor(question_id, difficulty, marks) {
        this.question_id = question_id;
        this.difficulty = difficulty;
        this.marks = marks;
    }
}

var questionInMem = {
    'easy' : [],
    'medium' : [],
    'hard' : [],
}

function addQuestion(questionJson) {

    var question_id = questionJson.question_id || null;
    var difficulty = questionJson.difficulty || null;
    var marks = questionJson.marks || null;

    if(!(question_id && difficulty && marks)) {
        return {
            status : 0,
            result : null,
            error : "invalid type"
        }
    }

    if(typeof marks != 'number' || !Number.isInteger(marks)) {
        return {
            status : 0,
            result : null,
            error : "invalid type marks"
        }
    }

    if(!Object.keys(questionInMem).includes(difficulty)) {
        return {
            status : 0,
            result : null,
            error : "invalid type diffculty"
        }
    }
    
    var question = new Question(questionJson.question_id, questionJson.difficulty, questionJson.marks);
    questionInMem[question.difficulty].push(question);
    return {
        status : 1,
        result : question_id,
        error : null
    }
}

function getQuestions(list, index, target, qList, dp) {

    if(dp && dp[index] && dp[index][target]) {
        return dp[index][target];
    }

    if(target == 0) {
        dp[index] = dp[index] || []; 
        dp[index][target] = qList;
        return qList;
    }

    if(index > list.length - 1) {
        dp[index] = dp[index] || [];
        dp[index][target] = null;
        return null;
    }

    var question = list[index];
    var newList = JSON.parse(JSON.stringify(qList));
    newList.push(question);

    var r =  getQuestions(list, index+1, target-question.marks, newList, dp) || getQuestions(list, index+1, target, qList, dp);
    dp[index] = dp[index] || [];
    dp[index][target] = r;
    return r;
}

function makeQuestionPool(total_marks, easy_percent, medium_percent, hard_percent) {

    if(!(typeof total_marks == 'number' && typeof easy_percent == 'number' && typeof medium_percent == 'number' && typeof hard_percent == 'number') ) {
        return {
            status : 0,
            result : null,
            error : "invalid input"
        }
    }

    if(100 != (easy_percent + medium_percent + hard_percent)) {
        return {
            status : 0,
            result : null,
            error : "sum of percentage is not 100"
        }
    }

    var total_marks_for_easy = (total_marks*easy_percent)/100;
    var total_marks_for_medium = (total_marks*medium_percent)/100;
    var total_marks_for_hard = (total_marks*hard_percent)/100;

    if(!(Number.isInteger(total_marks_for_easy) && Number.isInteger(total_marks_for_medium) && Number.isInteger(total_marks_for_hard))) {
        return {
            status : 0,
            result : null,
            error : "expected integer values for percentages"
        }
    }

    var list_of_easy_q = [];
    var list_of_medium_q = [];
    var list_of_hard_q = [];
    var dp = [];
    var question_pool_for_easy = getQuestions(questionInMem['easy'], 0, total_marks_for_easy, list_of_easy_q, dp);
    dp = [];
    var question_pool_for_medium = getQuestions(questionInMem['medium'], 0, total_marks_for_medium, list_of_medium_q, dp);
    dp = [];
    var question_pool_for_hard = getQuestions(questionInMem['hard'], 0, total_marks_for_hard, list_of_hard_q, dp);


    if(!question_pool_for_easy) {
        return {
            status : 0,
            result : null,
            error : "cant find the easy questions of desired percentage"
        }
    }
    if(!question_pool_for_medium) {
        return {
            status : 0,
            result : null,
            error : "cant find the medium questions of desired percentage"
        }
    }
    if(!question_pool_for_hard) {

        return {
            status : 0,
            result : null,
            error : "cant find the hard questions of desired percentage"
        }   
    }

    var resultList = {};
    resultList = {
        'easy' : JSON.stringify(question_pool_for_easy),
        'medium' : JSON.stringify(question_pool_for_medium),
        'hard' : JSON.stringify(question_pool_for_hard)
    }

    return {
        status : 1,
        result : resultList,
        error : null
    };
}


module.exports = {
    addQuestion : addQuestion,
    makeQuestionPool : makeQuestionPool
}


































// var questionDB = [

//     {question_id : 'Q1', difficulty : 'easy', marks : 5}, 
//     {question_id : 'Q2', difficulty : 'easy', marks : 3},
//     {question_id : 'Q3', difficulty : 'easy', marks : 5},
//     {question_id : 'Q4', difficulty : 'easy', marks : 4},
//     {question_id : 'Q5', difficulty : 'easy', marks : 10},
//     {question_id : 'Q6', difficulty : 'medium', marks : 15},
//     {question_id : 'Q7', difficulty : 'medium', marks : 10},
//     {question_id : 'Q8', difficulty : 'medium', marks : 10},
//     {question_id : 'Q9', difficulty : 'medium', marks : 16},
//     {question_id : 'Q10', difficulty : 'difficult', marks : 20},
//     {question_id : 'Q11', difficulty : 'difficult', marks : 25},
//     {question_id : 'Q12', difficulty : 'difficult', marks : 30}, ] 
    
    
    
// questionDB.forEach(q => {
//     addQuestion(q);
// });
// console.log(makeQuestionPool(100, 15, 35 ,50));


/* my test cases */

// var l = [   {question_id : 'Q1', difficulty : 'easy', marks : 5}, 
//             {question_id : 'Q2', difficulty : 'easy', marks : 3},
//             {question_id : 'Q3', difficulty : 'easy', marks : 5},
//         ];
// var response = [];
// var myList = getQuestions(l, 0, 2, response);
// console.log("myList", myList);
// // console.log(response);





