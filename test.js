var questionLogic = require('./index');

var questionDB = [

{question_id : 'Q1', difficulty : 'easy', marks : 5}, 
{question_id : 'Q2', difficulty : 'easy', marks : 3},
{question_id : 'Q3', difficulty : 'easy', marks : 5},
{question_id : 'Q4', difficulty : 'easy', marks : 4},
{question_id : 'Q5', difficulty : 'easy', marks : 10},
{question_id : 'Q13', difficulty : 'easy', marks : 10},
{question_id : 'Q6', difficulty : 'medium', marks : 15},
{question_id : 'Q7', difficulty : 'medium', marks : 10},
{question_id : 'Q8', difficulty : 'medium', marks : 10},
{question_id : 'Q9', difficulty : 'medium', marks : 16},
{question_id : 'Q10', difficulty : 'hard', marks : 20},
{question_id : 'Q11', difficulty : 'hard', marks : 10},
{question_id : 'Q12', difficulty : 'hard', marks : 10}, ] 


questionDB.forEach(q => {
    var q = questionLogic.addQuestion(q);
});


console.log(questionLogic.makeQuestionPool(100, 25, 35, 40));